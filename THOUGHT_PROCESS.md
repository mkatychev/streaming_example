# Plan

1. Create a line stream for the input file using `get_file_buf`
1. for every line of `abalone_full.csv`, call `Collector::check` to loop
   through every rule; if the `Line` passes at least one rule:
    * add the `Line` to `Collector.line_matches` map using the `Line.id` as a key
    * add the `Line.id` to the passed rules as a foreign key
1. once `Rule` evaltions are done:
    * use `Collector::write_matches` to output all rules as distinct CSVs to the provided output dir
1. Inside of `write_matches`:
    * createa a concurrent queue for every rule that will be pushed to and popped from
    * spawn 1+n `tasks` to:
        * (1) Loop through the matched lines in ascending order, adding lines to any rule queues that hold the id string reference
            * Once all matched lines are iterated through, close all queues
        * (n) For each rule:
            * a task is spawned that listens the corresponding queue to stream lines to the corresponding output file (provided by the rule name + `.csv`)
            * once the queue is closed finish the task

# Notes

### Grouping
* All `id`s accross the 3 output CSVs should be corellated
* use enumerated id and ring grouping as the same field for all outputs since it is a common denomitaor:

| id  | ... |
| --- | --- |
| 09r01 | ... |
| 13r01 | ... |
| 13r02 | ... |
| 14r01 | ... |
| 14r02 | ... |
| 14r03 | ... |

### Adjustable values
* <s>Maintain a config file that allows adjustment of more than `n` ring threshold and `Sex`</s>
* provide loosely coupled functions as basis of checking match, this way so long as an `Fn(&Line) -> bool` is provided, we can move the "domain logic" upstream.


### Errors
* is panicking the best way to propagate an unrecoverable error out of a task?
* Some suggestions in this [thread](https://users.rust-lang.org/t/propagating-errors-from-tokio-tasks/41723/2), but I am still not clear on it

### Data Structures
* Use `Mutex` for sharing matched lines and indices since there is no guarantee
* we will need to read _or_ modify (push to in this case), and wrap it in `Arc`
* for thread safety.
* <s>Use `BTreeMap` instead of `HashMap` for matched lines so that we can iterate in order of `id`</s>
    * order of insertion does not actually matter since we need to order by ring count first
#### MVP
* Avoid explicit lifetimes so use `String` instead of `&'a str` to avoid `<'a>` for now

### sync vs. async
* Seems that sync Mutex may be more beneficial than async Mutex
* [`ConcurrentQueue`](https://docs.rs/concurrent-queue/1.2.2/concurrent_queue/) a possible replacement for `Mutex<Vec<T>>`?


### Parallellism/concurrency attempts
* `async-rs/parallel-stream` [currently has issues with latest async-std](https://github.com/async-rs/parallel-stream/pull/21)
* `futures::stream::StreamExt` has [`for_each_concurrent`](https://docs.rs/futures/0.3.16/futures/stream/trait.StreamExt.html#method.for_each_concurrent) method but there will then be a trait import conflict. Worth looking into further


### Issues with implementation:
```rust
while !rule_queue.is_closed() || !rule_queue.is_empty() {
    if !rule_queue.is_empty() {
        let out_line = rule_queue.pop().unwrap();
        file.write(out_line.as_bytes()).await.unwrap();
        // adding sync_all significatly slows
        file.sync_all().await.unwrap();
    }
}
```
* Adding `File::sync_all` greatly slows down the async operation
