# Technical questions

Please answer the following questions in the markdown file called `FOLLOW-UP.md`.
It is ok if you don't know the answer for some of the questions, therefore don't copy/paste from Google.

1. How long did you spend on the coding test? What would you add to your solution if you had more time? _If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add._
2. What was the most useful feature added to the latest version of your chosen language? Please include a snippet of code that shows how you've used it.
3. How would you track down a performance issue in production? Have you ever had to do this?
4. How would you monitor and trace issues in a distributed systems environment?
