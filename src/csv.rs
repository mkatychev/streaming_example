use std::{
    collections::{HashMap, HashSet},
    convert::{TryFrom, TryInto},
    hash::{Hash, Hasher},
};

use concurrent_queue::ConcurrentQueue;
use futures::future;
// use parallel_stream::prelude::*;

use async_std::{
    fs::File,
    io::{prelude::*, BufReader},
    path::{Path, PathBuf},
    stream::{self, StreamExt},
    sync::{Arc, Mutex},
    task,
};

const OUTPUT_HEADER: &str = "id,Sex,Length,Diameter,Height,Whole_weight,Shucked_weight,Viscera_weight,Shell_weight,Class_number_of_rings";

pub async fn process_abalones<T: AsRef<Path>>(
    input: T,
    rules: Vec<Rule>,
) -> Result<Collector, AsyncError> {
    let mut file = get_file_buf(input).await?;
    // skip header line
    file.read_line(&mut String::new()).await?;
    let collector = Arc::new(Collector::new(rules));

    let mut lines = file.lines();
    let mut row: u32 = 0;

    while let Some(line) = lines.next().await {
        // we increment at the start since we dropped the header
        // this should make row incrementing synchronous on stream (I hope)
        row += 1;

        let line = Line::try_from((row, line?))?;
        let collector = Arc::clone(&collector);

        task::spawn(async move {
            collector.check(line).await;
        });
    }

    Arc::try_unwrap(collector)
        .map_err(|_| "Arc<Collector> has more than one strong reference".into())
}

pub type AsyncError = Box<dyn std::error::Error + Send + Sync>;

pub async fn get_file_buf<T: AsRef<Path>>(path: T) -> Result<BufReader<File>, AsyncError> {
    let file = File::open(path.as_ref()).await?;
    Ok(BufReader::new(file))
}

// Checker is a function that returns true if there is a match for the rule
pub type Checker = Box<dyn Fn(&Line) -> bool + Send + Sync>;

pub struct Rule {
    name:        String, // Rule name/output filename
    is_match:    Checker,
    idx_matches: Mutex<HashSet<String>>, // stores ID ref of Matcher.lines
}

impl Rule {
    pub fn new(name: String, is_match: Checker) -> Self {
        Self {
            name,
            is_match,
            idx_matches: Mutex::new(HashSet::new()),
        }
    }

    // once the Checker is no longer used, drop the Mutex and return a simple tuple
    fn decompose(self) -> (HashSet<String>, String) {
        (self.idx_matches.into_inner(), self.name)
    }
}

pub struct Collector {
    rules:        Vec<Rule>,
    line_matches: Mutex<HashMap<String, Line>>,
}

impl Collector {
    pub fn new(rules: Vec<Rule>) -> Self {
        Self {
            rules,
            line_matches: Mutex::new(HashMap::new()),
        }
    }

    // check verifies that if a [Line] passes a [Checker] criteria
    // 1. the row index is stored in the corresponding [Rule]
    // 2. the Line is stored with a map in the [Collector] using the row index as a key
    async fn check(&self, line: Line) {
        // don't think this needs to be a stream
        let mut s = stream::from_iter(&self.rules);
        while let Some(rule) = s.next().await {
            if (rule.is_match)(&line) {
                // store index of line that matches rule
                let mut id_matches = rule.idx_matches.lock().await;
                id_matches.insert(line.id.clone());

                let mut line_matches = self.line_matches.lock().await;
                // commit line reference on first match
                if !line_matches.contains_key(&line.id) {
                    // line will be derefed after while iteration is finished
                    // so clone is not so bad
                    line_matches.insert(line.id.clone(), line.clone());
                }
            }
        }
    }

    pub async fn write_matches<P: AsRef<Path>>(self, out_dir: P) -> Result<(), AsyncError> {
        // consume rules
        let rules: Vec<(HashSet<String>, String)> =
            self.rules.into_iter().map(|r| r.decompose()).collect();
        // consume mutex
        let lines = self.line_matches.into_inner();
        let mut ids: Vec<String> = lines.keys().cloned().collect();
        // iterate by id
        ids.sort();

        let queues = Arc::new(
            rules
                .iter()
                .map(|(_, rule_name)| (rule_name.clone(), ConcurrentQueue::unbounded()))
                .collect::<HashMap<String, ConcurrentQueue<String>>>(),
        );

        // spawn write streams for each rule
        let mut futs: Vec<task::JoinHandle<_>> = rules
            .clone()
            .into_iter()
            .map(|(_, name)| {
                let fpath = PathBuf::from(out_dir.as_ref()).join(format!("{}.csv", name));
                let queues = Arc::clone(&queues);
                task::spawn(async move {
                    let mut file = File::create(fpath).await.unwrap();

                    let key = name.clone();
                    let rule_queue = &queues[&key];
                    file.write(format!("{}\n", OUTPUT_HEADER).as_bytes())
                        .await
                        .unwrap();

                    // while queue is open OR queue is not empty
                    while !rule_queue.is_closed() || !rule_queue.is_empty() {
                        if !rule_queue.is_empty() {
                            let out_line = rule_queue.pop().unwrap();
                            file.write_all(out_line.as_bytes()).await.unwrap();
                            // adding sync_all significatly slows
                            // file.sync_all().await.unwrap();
                        }
                    }
                })
            })
            .collect();

        // we want to be synchronous here so we insert each row in the desired order
        // let push_queues = Arc::clone(&queues);
        let queue_pusher = task::spawn(async move {
            for (i, id) in ids.iter().enumerate() {
                for (set, rule_name) in rules.iter() {
                    if set.contains(id) {
                        queues[rule_name].push(lines[id].writeable()).unwrap();
                    }
                }
                // close all queues if we are on last enumerable
                if i as usize == ids.len() - 1 {
                    for (_, q) in queues.iter() {
                        q.close();
                    }
                }
            }
        });
        futs.push(queue_pusher);

        future::join_all(futs).await;
        Ok(())
    }
}

#[derive(Clone, Debug, PartialEq)]
pub enum Sex {
    Male,
    Female,
    Infant,
}

impl TryFrom<&str> for Sex {
    type Error = String;

    fn try_from(sex: &str) -> Result<Self, String> {
        match sex.chars().next() {
            Some('M') => Ok(Self::Male),
            Some('F') => Ok(Self::Female),
            Some('I') => Ok(Self::Infant),
            Some(_) => Err(format!("invalid sex: {}", sex)),
            None => Err("Sex::TryFrom cannot take an empty string".into()),
        }
    }
}

#[derive(Clone, Debug)]
pub struct Line {
    pub id:             String,
    pub raw_line:       String,
    pub row:            u32,
    pub sex:            Sex,
    pub length:         f32,
    pub diameter:       f32,
    pub height:         f32,
    pub whole_weight:   f32,
    pub shucked_weight: f32,
    pub viscera_weight: f32,
    pub shell_weight:   f32,
    pub rings:          u32,
}

impl Line {
    fn writeable(&self) -> String {
        format!("{},{}\n", self.id, self.raw_line)
    }
}

impl PartialEq for Line {
    fn eq(&self, other: &Self) -> bool {
        self.raw_line == other.raw_line
    }
}

impl Eq for Line {}

impl Hash for Line {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.raw_line.hash(state);
        // add id to ensure that identical data for different abalones do not collide
        self.row.hash(state);
    }
}

impl TryFrom<(u32, String)> for Line {
    type Error = AsyncError;

    fn try_from((i, line): (u32, String)) -> Result<Self, Self::Error> {
        let cols: Vec<&str> = line.split(',').collect();
        if cols.len() != 9 {
            return Err("comman delimited strings do not amount to 9".into());
        }

        // let rings be formatted as an integer
        let rings = cols[8].parse()?;
        Ok(Self {
            id:             format!("{:02}r{}", rings, i), // ring count + CSV row index
            row:            i,
            sex:            cols[0].try_into()?,
            length:         cols[1].parse()?,
            diameter:       cols[2].parse()?,
            height:         cols[3].parse()?,
            whole_weight:   cols[4].parse()?,
            shucked_weight: cols[5].parse()?,
            viscera_weight: cols[6].parse()?,
            shell_weight:   cols[7].parse()?,
            raw_line:       line,
            rings:          rings,
        })
    }
}
