mod csv;

use csv::{process_abalones, Checker, Line, Rule, Sex};
use std::{fs, path::PathBuf};

// NOTE all *_floor are exclusive

fn infants_with_more_than_n_rings(ring_floor: u32) -> Checker {
    Box::new(move |line: &Line| line.rings > ring_floor && line.sex == Sex::Infant)
}

fn males_heavy_and_short(weight_floor: f32, length_limit: f32) -> Checker {
    Box::new(move |line: &Line| {
        line.whole_weight > weight_floor && line.length < length_limit && line.sex == Sex::Male
    })
}

// TODO add collector for additional column
// fn shell_humidity(line: &Line, weight_floor: f32, length_limit: f32) -> Checker {
//     Box::new(move |line| line.whole_weight > weight_floor && line.length < length_limit)
// }

#[async_std::main]
async fn main() -> Result<(), csv::AsyncError> {
    let args: Vec<String> = std::env::args().collect();
    println!("{}", &args[1]);
    println!("{}", &args[2]);
    let input = fs::canonicalize(PathBuf::from(&args[1]))?;
    let out_dir = fs::canonicalize(PathBuf::from(&args[2]))?;

    if !input.is_file() {
        return Err(format!("{:?} is not a file", input).into());
    }

    if !out_dir.is_dir() {
        return Err(format!("{:?} is not a directory", out_dir).into());
    }

    let rules = vec![
        Rule::new(
            "infants_with_more_than_14_rings".into(),
            infants_with_more_than_n_rings(14),
        ),
        Rule::new(
            "males_heavy_and_short".into(),
            males_heavy_and_short(0.4, 0.5),
        ),
    ];
    let collector = process_abalones(input, rules).await?;
    collector.write_matches(out_dir).await?;

    Ok(())
}
