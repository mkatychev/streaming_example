# Abalone Checker

### Running csv processor:
`cargo run -- <input_csv> <output_dir>`


### About rules
`Rule`s are defined by the combination of a name (that gets `.csv` appended to
it for file creation) and a `Checker` (`Fn(&Line) -> bool`) function that takes a `&Line` argument
retuning a boolean:

```rust
// main.rs
let rules = vec![
    Rule::new(
        "infants_with_more_than_14_rings".into(),
        infants_with_more_than_n_rings(14),
    ),
    Rule::new(
        "males_heavy_and_short".into(),
        males_heavy_and_short(0.4, 0.5),
    ),
];
```
